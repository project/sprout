;
; Sprout
;
; Defines a base install profile that includes default customizations providing
; a well thought out client administration interface and features to make
; development easier.
;

api                                          = 2
core                                         = 7.x

projects[]                                   = "drupal"

;
; DEVELOPMENT
;
; These are modules useful during the development process.
;

projects[devel][subdir]                      = "devel"
projects[performance][subdir]                = "devel"
projects[reroute_email][subdir]              = "devel"
projects[stage_file_proxy][subdir]           = "devel"
projects[search_krumo][subdir]               = "devel"
projects[styleguide][subdir]                 = "devel"

;
; DEVELOPER EXPERIENCE
;
; These are modules useful for enhancing the developer experience.
;

projects[module_filter][subdir]              = "devel"
projects[fpa][subdir]                        = "devel"

;
; CLIENT ADMINISTRATION
;
; These are modules useful for a clients' administration of your site.
;

projects[advanced_help][subdir]              = "contrib"
projects[scheduler][subdir]                  = "contrib"
projects[views_ui_basic][subdir]             = "contrib"
projects[ccl][subdir]                        = "contrib"
projects[linkit][subdir]                     = "contrib"
projects[admin_views][subdir]                = "contrib"
projects[better_formats][subdir]             = "contrib"
projects[pagepreview][subdir]                = "contrib"
projects[panels][subdir]                     = "contrib"
projects[node_form_panes][subdir]            = "contrib"
projects[save_draft][subdir]                 = "contrib"
projects[override_node_options][subdir]      = "contrib"
projects[view_unpublished][subdir]           = "contrib"
projects[options_element][subdir]            = "contrib"
projects[total_control][subdir]              = "contrib"
projects[date_popup_authored][subdir]        = "contrib"

projects[menu_admin_per_menu][subdir]        = "contrib"
projects[mpac][subdir]                       = "contrib"
projects[simplified_menu_admin][subdir]      = "contrib"
projects[chain_menu_access][subdir]          = "contrib"
projects[administerusersbyrole][subdir]      = "contrib"
projects[role_delegation][subdir]            = "contrib"
projects[userprotect][subdir]                = "contrib"
projects[escape_admin][subdir]               = "contrib"

;
; Sprout Features
;

projects[sprout_admin][subdir]               = "sprout"
projects[sprout_core][subdir]                = "sprout"
projects[sprout_devel][subdir]               = "sprout"
projects[sprout_media][subdir]               = "sprout"
projects[sprout_wysiwyg][subdir]             = "sprout"
projects[sprout_users][subdir]               = "sprout"

;
; File uploads and handling
;

projects[imce][subdir]                       = "contrib"
projects[imce_mkdir][subdir]                 = "contrib"
projects[imce_filefield][subdir]             = "contrib"
projects[imce_plupload][subdir]              = "contrib"
projects[imce_tools][subdir]                 = "contrib"
projects[plupload][subdir]                   = "contrib"
libraries[plupload][type]                    = "library"
libraries[plupload][download][type]          = get
libraries[plupload][download][url]           = https://github.com/moxiecode/plupload/archive/v1.5.8.zip
libraries[plupload][directory_name]          = plupload
projects[image_resize_filter][subdir]        = "contrib"

;
; WYSIWYG Editor (CKEditor)
;
projects[ckeditor][subdir]                   = "contrib"

;
; CKEditor plugins
;
; Actually, the build the CKEditor module comes with (from the CKEditor CDN)
; is really not too bad a starting point. So instead we'll just add some
; plugins here
;

; The YouTube Plugin is actually quite awesome
libraries[ckeditor_youtube][download][type]          = "get"
libraries[ckeditor_youtube][download][url]           = "http://download.ckeditor.com/youtube/releases/youtube_2.0.4.zip"
libraries[ckeditor_youtube][directory_name]          = "youtube"
libraries[ckeditor_youtube][download][subtree]       = "youtube"
libraries[ckeditor_youtube][destination]             = "libraries/ckeditor/plugins"

; This guy formats your html in "source" mode which is nice
libraries[ckeditor_codemirror][download][type]          = "get"
libraries[ckeditor_codemirror][download][url]           = "http://download.ckeditor.com/codemirror/releases/codemirror_1.13.zip"
libraries[ckeditor_codemirror][download][subtree]       = "codemirror"
libraries[ckeditor_codemirror][directory_name]          = "codemirror"
libraries[ckeditor_codemirror][destination]             = "libraries/ckeditor/plugins"

;
; CHOSEN
;
projects[chosen][subdir]    				         = "contrib"
; Chosen Library
libraries[chosen][download][type]          = "get"
libraries[chosen][download][url]           = "https://github.com/harvesthq/chosen/releases/download/v1.1.0/chosen_v1.1.0.zip"
libraries[chosen][destination]             = "libraries"

;
; SUPPORTING CAST
;
; These are modules that are depended on for other module's functionality, but
; generally have no functionality on their own.
;

projects[libraries][subdir]                  = "contrib"
projects[ctools][subdir]                     = "contrib"
projects[entity][subdir]                     = "contrib"
projects[file_entity][subdir]                = "contrib"
projects[token][subdir]                      = "contrib"

;
; FIELDS
;
; All the field related modules.
;

projects[transliteration][subdir]            = "contrib"
projects[cck][subdir]                        = "contrib"
projects[entityreference][subdir]            = "contrib"
projects[inline_entity_form][subdir]         = "contrib"
projects[link][subdir]                       = "contrib"
projects[date][subdir]                       = "contrib"
projects[field_group][subdir]                = "contrib"
projects[filefield_paths][subdir]            = "contrib"

;
; Responsive Images
;

projects[picture][subdir]                    = "contrib"
projects[breakpoints][subdir]                = "contrib"

;
; SEO AND ANALYTICS
;
; Search engine optimization modules including nice paths.
;

projects[pathauto][subdir]                   = "contrib"
projects[metatag][subdir]                    = "contrib"
projects[redirect][subdir]                   = "contrib"
projects[redirect][version]                  = "1.x-dev"
projects[globalredirect][subdir]             = "contrib"
projects[google_analytics][subdir]           = "contrib"

;
; VIEWS
;
; Views and related modules.
;

projects[views][subdir]                      = "contrib"
projects[views_slideshow][subdir]            = "contrib"
projects[views_bulk_operations][subdir]      = "contrib"
projects[flexslider][subdir]                 = "contrib"
libraries[flexslider][download][type]        = "get"
libraries[flexslider][download][url]         = "https://github.com/woothemes/FlexSlider/zipball/master"
libraries[flexslider][directory_name]        = "flexslider"
libraries[flexslider][type]                  = "library"

;
; CODE DRIVEN DEVELOPMENT
;
; Modules that are needed to implement a full featured code driven
; development paradigm.
;

projects[features][subdir]                   = "contrib"
projects[features_extra][subdir]             = "contrib"
projects[uuid][subdir]                       = "contrib"
projects[uuid_features][subdir]              = "contrib"
projects[strongarm][subdir]                  = "contrib"
projects[context][subdir]                    = "contrib"
projects[diff][subdir]                       = "devel"

;
; USERS
;
projects[shortcutperrole][subdir]            = "contrib"
projects[logintoboggan][subdir]              = "contrib"

;
; ADDITIONAL MODULES
;

projects[webform][subdir]                    = "contrib"
projects[rabbit_hole][subdir]                = "contrib"
projects[content_type_extras][subdir]        = "contrib"
projects[jquery_update][subdir]              = "contrib"
projects[icon_tabs][subdir]                  = "contrib"

;
; THEMES
;
; All the themes we need including our base theme ready to go.
;

projects[tao][type]                          = theme
projects[rubik][type]                        = theme

;
; PATCHES
;

;
; Further simlifies the edit menu screen by hiding the machine name and menu description.
; https://drupal.org/node/2008378
;
projects[simplified_menu_admin][version]     = "1.x-dev"
projects[simplified_menu_admin][patch][]     = https://drupal.org/files/more_simplified_edit_menu_screen-2008378-2.patch

;
; Simplifies the edit link screen by remmoving the enabled link on new menu items
; and hiding the weight field always.
; https://drupal.org/node/2008396
;
projects[simplified_menu_admin][version]     = "1.x-dev"
projects[simplified_menu_admin][patch][]     = https://drupal.org/files/simplify_the_edit_link_screen-2008396-2.patch

