<?php
/**
 * Implement hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * For now we're using the standard installation which creates a few content types
 * and adds a role for administrators.
 *
 *
 */
function sprout_install() {
  // Tells Drupal about our installation profile.
  $settings_file = realpath(conf_path() . '/settings.php');
  drupal_verify_install_file($settings_file, FILE_READABLE|FILE_WRITABLE);
  $fp = fopen($settings_file, "a");
  fwrite($fp, "\n\n\$conf['install_profile'] = 'sprout';\n");
  fclose($fp);
  drupal_verify_install_file($settings_file, FILE_READABLE|FILE_NOT_WRITABLE);

  variable_set('admin_theme', 'rubik');
  variable_set('node_admin_theme', '1');
}
